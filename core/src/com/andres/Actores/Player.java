package com.andres.Actores;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Red Pine on 12/02/2018.
 */

public class Player extends Actor {
    Texture imgjugador;

    public Player(Texture imgjugador){
        this.imgjugador = imgjugador;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }
}
