package com.andres;

import com.andres.Actores.Player;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.Game;

public class Firstgame extends Game {

	float  frame=0.1f;
	SpriteBatch batch;
	Texture img;
	Texture pj, boss;
	TextureRegion textureRegion;

	Player player;
	@Override
	public void create () {

		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");
		boss = new Texture("dandelion.png");
	  	pj = new Texture("spirit.png");

		player = new Player(pj);
		
	  	player.setPosition(40, 40);
	  	player.setHeight(1.5f);

	  	textureRegion = new TextureRegion(boss, 0, 0, 50, 50);
	}

	@Override
	public void setScreen(Screen screen) {
		super.setScreen(screen);
	}

	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(1, 1, 1, 1);

		int height = Gdx.graphics.getHeight();
		int width = Gdx.graphics.getWidth();
		//System.out.println(height+" --- "+width);
		int imgAlto = pj.getHeight();
		int imgAncho = pj.getWidth();

		if(Gdx.input.justTouched())
			System.out.println("*FRAME:*"+frame);



		batch.begin();
		//batch.draw(pj, 0,0);
		batch.draw(textureRegion, width-42, 0);
		batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
		pj.dispose();
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		System.out.println("***||| SE REINICIO**");
		super.resume();
	}
}
